FROM rust:1.53 as builder
WORKDIR /usr/src

# Preparation for static linking.
RUN apt-get update && \
    apt-get dist-upgrade -y && \
    apt-get install -y musl-tools && \
    rustup target add x86_64-unknown-linux-musl

# Download and compilation of Rust dependencies.
RUN cargo new dele
WORKDIR /usr/src/dele
COPY Cargo.toml Cargo.lock ./
RUN cargo build --release --target x86_64-unknown-linux-musl && \
    rm -rf ./src

# Build the exe using the actual source code.
COPY src ./src
RUN cargo clean && \
    cargo build --release --target x86_64-unknown-linux-musl

# Copy the exe to an empty Docker image
FROM alpine as final
RUN mkdir /app
VOLUME /app
COPY --from=builder /usr/src/dele/target/x86_64-unknown-linux-musl/release/dele  /

CMD ["/dele"]
