# Dele

Dele is an application to split a big file in multiple small files.
Later on the files can be joined into the original file.


## Installation
> cargo install dele

## Usage
### Split a file
> dele split -f ./path/to/file -p number_of_parts

The "-f" argument represents the path to the file which will be divided into parts. The "-p" argument represents the number of divisions.

For example the following command
> dele split -f rust.png -p 7

will divide the file rust.png into 7 different files:

- rust.png0
- rust.png1
- ...
- rust.png6

### Join files
> dele join -f path/to/first_element

For example:
> dele join -f rust.png0

The command expects the first file of a previous partition.

## Docker

To fetch the latest container run:
> docker pull jorgarga/dele:latest

To split/divide a file located in PWD/test run:
> docker run --rm -it -v "${PWD}":/app jorgarga/dele /dele split -f /app/test/rust_orig.png -p 5

To join a set of split files in PWD/test run:
> docker run --rm -it -v "${PWD}":/app jorgarga/dele /dele join -f /app/test/rust_orig.png0



# Name

The name comes from the Norwegian term "å dele", which means both to split and to share.

