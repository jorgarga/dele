#!/bin/bash 

cargo build --release

cd test
cp rust_orig.png rust.png

../target/release/dele split -f rust.png -p 7

cat rust.png0 > rep.png
cat rust.png1 >> rep.png
cat rust.png2 >> rep.png
cat rust.png3 >> rep.png
cat rust.png4 >> rep.png
cat rust.png5 >> rep.png
cat rust.png6 >> rep.png

rm rust.png
../target/release/dele join -f rust.png0

ORIGINAL_SHA="$(sha512sum rust_orig.png | awk '{print $1}')"
CAT_SHA="$(sha512sum rep.png | awk '{print $1}')"
DELE_SHA="$(sha512sum rust.png | awk '{print $1}')"

rm rust.png*
rm rep.png

if [[ "${ORIGINAL_SHA}" = "${DELE_SHA}" && "${ORIGINAL_SHA}" = "${CAT_SHA}" ]]; then
    echo "Success! sha512sum: ${ORIGINAL_SHA}"
    exit 0
fi


echo "Test failed"
echo "Original sha512sum: ${ORIGINAL_SHA}"
echo "Dele sha512sum:     ${DELE_SHA}"
echo "Cat sha512sum:      ${CAT_SHA}"
