use clap::{App, Arg, SubCommand};
use log::trace;
use simple_logger::SimpleLogger;
use std::fs;
use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::io::{Error, SeekFrom};

fn filesize(path: &str) -> Result<u64, std::io::Error> {
    Ok(fs::metadata(path)?.len())
}

fn read_buffer(
    file: &mut File,
    bytes_per_part: u64,
    start: u64,
) -> Result<Vec<u8>, std::io::Error> {
    let mut buffer = vec![0; bytes_per_part as usize];
    file.seek(SeekFrom::Start(start))?;
    file.read_exact(&mut buffer)?;
    Ok(buffer)
}

fn read_buffer_to_end(
    file: &mut File,
    start: u64,
) -> Result<Vec<u8>, std::io::Error> {
    let mut buffer = Vec::new();
    file.seek(SeekFrom::Start(start))?;
    file.read_to_end(&mut buffer)?;
    Ok(buffer)
}

fn write_buffer(filename: String, buffer: &[u8]) -> Result<usize, std::io::Error> {
    let mut dest = File::create(filename)?;
    let written = dest.write(&buffer)?;
    Ok(written)
}

fn read_and_write(
    origin: &str,
    bytes_per_part: u64,
    mut file: &mut File,
    part: u64,
    is_last: bool,
) -> Result<usize, Error> {
    let start = bytes_per_part * part;
    let end = bytes_per_part * (part + 1);
    let filename = format!("{}{}", origin, part);

    trace!(
        "Part {}. Starting position: {}, Ending position: {}",
        part,
        start,
        end
    );

    let result = match is_last {
        true => read_buffer_to_end(&mut file, start),
        false => read_buffer(&mut file, bytes_per_part, start),
    };

    let buffer = result?;
    write_buffer(filename, &buffer)
}

fn split(origin: String, parts: u64, bytes_per_part: u64) -> io::Result<usize> {
    let mut file = File::open(&origin)?;
    let mut written = 0;

    for part in 0..parts - 1 {
        written += read_and_write(&origin, bytes_per_part, &mut file, part, false)?;
    }

    written += read_and_write(&origin, bytes_per_part, &mut file, parts - 1, true)?;

    Ok(written)
}

fn read_file_content(path: &str) -> Result<Vec<u8>, std::io::Error> {
    let mut f = File::open(path)?;
    let mut buffer = Vec::new();

    f.read_to_end(&mut buffer)?;
    Ok(buffer)
}

fn join(part0: String) -> Result<usize, std::io::Error> {
    let dest_name: String = (&part0[..part0.len() - 1]).to_string();
    let mut counter: u32 = part0.chars().last().unwrap().to_digit(10).unwrap();

    trace!("Destination: {}", dest_name);
    let mut to_file = File::create(&dest_name).unwrap();
    let mut total_read = 0;

    loop {
        let from_file = format!("{}{}", &dest_name, &counter.to_string());
        let result = read_file_content(&from_file);

        if result.is_err() {
            trace!("No more split files found. Process done!");
            break;
        }

        let content = result.unwrap();
        to_file.write_all(&content)?;

        total_read += content.len();
        trace!("{} bytes read from {}", content.len(), from_file);

        counter += 1;
    }

    Ok(total_read)
}

fn main() {
    SimpleLogger::new().init().unwrap();

    trace!("Start");

    let matches = App::new("Dele")
        .version("0.1")
        .about("Splits files to join them later. Useful when files are too big for a medium.")
        .subcommand(
            SubCommand::with_name("split")
                .about("Splits a file into smaller files.")
                .arg(
                    Arg::with_name("filename")
                        .value_name("FILE")
                        .short("f")
                        .required(true)
                        .help("File to be divided into parts."),
                )
                .arg(
                    Arg::with_name("parts")
                        .short("p")
                        .value_name("PARTS")
                        .required(true)
                        .help("Number of parts it should be divided."),
                ),
        )
        .subcommand(
            SubCommand::with_name("join")
                .about("Joins many files (previously split) into the original file.")
                .arg(
                    Arg::with_name("filename")
                        .value_name("FILE")
                        .short("f")
                        .required(true)
                        .help("File to be divided into parts."),
                ),
        )
        .get_matches();

    if let Some(matches) = matches.subcommand_matches("split") {
        let origin: String = matches.value_of("filename").unwrap().to_string();
        let parts: u64 = matches.value_of("parts").unwrap().parse::<u64>().unwrap();
        trace!("File {} will be divided into {} parts.", origin, parts);

        let size_bytes = filesize(&origin).unwrap() as f32;
        let bytes_per_part = (size_bytes / parts as f32).ceil() as u64;

        trace!(
            "Origin: {} ({} bytes) will be split in {} parts ({} bytes/part).",
            origin,
            size_bytes,
            parts,
            bytes_per_part
        );

        let written = split(origin, parts, bytes_per_part).unwrap();
        trace!("Written: {} bytes", written);
    }

    if let Some(matches) = matches.subcommand_matches("join") {
        let part0: String = matches.value_of("filename").unwrap().to_string();
        let total_read = join(part0).unwrap();
        trace!("Total bytes read: {}", total_read)
    }
}
